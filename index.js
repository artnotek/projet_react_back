import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import router from "./src/routes/routes";
import mongoose from 'mongoose';
import database from "./src/models/database";
// import jwt from "src/config/jwt"
// init
const app = express();

// // JWT
// app.use(jwt());

// config
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(cors({origin:true}));

// use route
app.use(router);

// launch
const port = 3005;

// mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true});
// let db = mongoose.connection;
// db.on('error', console.error.bind(console, 'connection error:'));
// db.once('open', function() {
//     console.log('connecté');
// });
database.connectDb().then(() => {
    console.log('Database server is connected...');
    app.listen(port, () => {
        console.log(`Server listening on port ${port}`);
    })
});

// app.listen(port, () => {
//     console.log(`Server listening on port ${port}`);
// });
