import Drawing from "../models/Drawing";
import Party from "../models/Party";
import User from "../models/User";

class DrawingController {

    /**
     * function to send a new drawing after user has finished drawing
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async create(req,res){
        let status = 200;
        let body = {};

        try {
            let drawing = await Drawing.create({
                link: req.body.link,
                rawDrawingInfo: req.body.rawDrawingInfo
            });
            let getBelongingDrawingParty = await Party.findById(req.body.partyId);
            let getDrawingUserId = await User.findById(req.body.userId);
            console.log('+------- DEBUT DES DATAS ---------+');
            console.log(drawing);
            console.log('--------');
            console.log(getBelongingDrawingParty);
            console.log('--------');
            console.log(getDrawingUserId);
            let addDrawingToBelongingParty = await Party.findOneAndUpdate(
                {_id: getBelongingDrawingParty._id},
                {
                    $push:{
                        drawings: {
                            drawingLocationId: drawing,
                            userId: getDrawingUserId
                        }
                    }
                },
                {new: true});
            console.log('--------');
            console.log(addDrawingToBelongingParty);
            console.log('+------- FIN DES DATAS ---------+');
            body = {
                'message': `Drawing ${drawing._id} created and added to the party  ${getBelongingDrawingParty.name}`,
                drawing
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * function who get details from selected drawing to fetch
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let drawing = await Drawing.findById(id);

            body = {
                'message': `Detail from drawing : ${drawing._id}`,
                drawing
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }
}

export default DrawingController;
