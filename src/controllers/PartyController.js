import Party from "../models/Party";
import User from "../models/User";

class PartyController {
    /**
     * function to initiate a new game session for other to join
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async createNewSession(req,res){
        let status = 200;
        let body = {};

        try {
            let party = await Party.create({
                name: req.body.name,
                theme: req.body.theme,
                creatorId: req.body.creatorId
            });
            body = {
                'message': `Party ${party.name} created `,
                party
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * function to allow people to be in the session for drawing next
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async getIntoGame(req,res){
        let status = 200;
        let body = {};

        try {
            let selectedPartyData = await Party.findById(req.body.partyId);
            console.log('+------- DEBUT DES DATAS ---------+');
            console.log(selectedPartyData);
            let joingingUserData = await User.findById(req.body.userId);
            console.log(joingingUserData);
            console.log('+------- FIN DES DATAS ---------+');


            console.log(selectedPartyData._id);
            console.log(req.body.partyId);
            let party = await Party.findOneAndUpdate(
                {_id: selectedPartyData._id},
                {
                    $push:{
                        players: {
                            userId: joingingUserData,
                            isModerated: false
                        }
                    }
                    // name: selectedPartyData.name,
                    // theme: selectedPartyData.theme,
                    // creatorId: selectedPartyData.creatorId,
                },
                {new: true});
            body = {
                'message': `Player ${joingingUserData.surname} added to the party `,
                party
            }
            console.log('+------- DEBUT DES DATAS UPDATED ---------+');
            console.log(party);
            console.log(party.players);
            console.log('+------- FIN DES DATAS UPDATED ---------+');

        }
        catch (error) {
            status = 404;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }


    /**
     * function to fetch all created game party and display them
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async findAll(req, res) {
        let status = 200;
        let body = {};

        try {
            let parties = await Party.find();
            console.log(parties);
            body = {
                parties,
                'message': 'Parties list'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    /**
     * function to get all details from selected party: Users & drawings
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let party = await Party.findById(id);

            body = {
                'message': `Detail from party's theme : ${party.theme}`,
                party
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    /**
     * function for party owner only, hide an undesired username from session
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async moderateSelectedUser(req,res){
        let status = 200;
        let body = {};

        try {
            let moderateUser = await Party.findOneAndUpdate({
                "_id": req.body.partyId,
                "players._id": req.body.playerId
            },{
                "$set": {
                    "players.$.isModerated": true
                }
            },
            {new: true});
            body = {
                'message': `User ${moderateUser} moderated from the session `,
            }
        }
        catch (error) {
            status = 404;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }


    /**
     * function for party owner only, revoke a hidden username
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async unModerateSelectedUser(req,res){
        let status = 200;
        let body = {};

        try {
            let moderateUser = await Party.findOneAndUpdate({
                "_id": req.body.partyId,
                "players._id": req.body.playerId
            },{
                "$set": {
                    "players.$.isModerated": false
                }
            },
            {new: true});
            body = {
                'message': `User ${moderateUser} cleared from the session `,
            }
        }
        catch (error) {
            status = 404;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * function for party owner only, hide a undesired drawing proposal
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async moderateSelectedDrawing(req,res){
        let status = 200;
        let body = {};

        try {
            let moderateUser = await Party.findOneAndUpdate({
                "_id": req.body.partyId,
                "drawings._id": req.body.drawingId
            },{
                "$set": {
                    "drawings.$.isModerated": true
                }
            },
            {new: true});
            body = {
                'message': `drawing ${moderateUser} moderated from the session `,
            }
        }
        catch (error) {
            status = 404;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * function for owner only, revoke a hidden drawing
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async unModerateSelectedDrawing(req,res){
        let status = 200;
        let body = {};

        try {
            let moderateUser = await Party.findOneAndUpdate({
                "_id": req.body.partyId,
                "drawings._id": req.body.drawingId
            },{
                "$set": {
                    "drawings.$.isModerated": false
                }
            },
            {new: true});
            body = {
                'message': `drawing ${moderateUser} cleared from the session `,
            }
        }
        catch (error) {
            status = 404;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * function for owner party only, end the selected session and put said session in archive
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async endSession(req,res){
        let status = 200;
        let body = {};

        try {
            console.log(req);
            let userIdForVerification = await User.findById(req.body.userId);
            let selectedPartyData = await Party.findById(req.body.partyId);
            let drawingSessionsToEnd = await Party.findOneAndUpdate(
                {_id: req.body.partyId},
                {
                    $set:{
                        isActive : false
                    }

                },
                {new: true});
            body = {
                'message': `Party ${selectedPartyData.theme} ended and archived `,
            }
        }
        catch (error) {
            status = 404;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * dev test function, will be removed
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async unmoderateUserName(req,res){
        let status = 200;
        let body = {};

        try {
            let userIdForVerification = await User.findById(req.body.userId);
            let getSelectedPartyData = await Party.findById(req.body.partyId);
            let selectedPartyData = await Party.update(
                {
                        _id : req.body.partyId, players: {
                            _id: req.body.userId
                        }
                    },
                {
                    $set:{
                        isModerated: false
                    }

                },
                {new: true});
            console.log('+------- DEBUT DES DATAS ---------+');
            // console.log(userIdForVerification);
            // console.log('---------------');
            // console.log(getSelectedPartyData);
            // console.log('---------------');
            console.log(selectedPartyData.players);
            console.log('+------- FIN DES DATAS ---------+');
            body = {
                'message': `User ${selectedPartyData.name} updated and status unmodered `,
            }
        }
        catch (error) {
            status = 404;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * dev test function, will be removed
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async unmoderateSelectedDrawing(req,res){
        let status = 200;
        let body = {};

        try {
            let userIdForVerification = await User.findById(req.body.userId);
            let getSelectedPartyData = await Party.findById(req.body.partyId);
            let selectedPartyData = await Party.update(
                {
                    _id : req.body.partyId, players: {
                        _id: req.body.userId
                    }
                },
                {
                    $set:{
                        isModerated: false
                    }

                },
                {new: true});
            console.log('+------- DEBUT DES DATAS ---------+');
            // console.log(userIdForVerification);
            // console.log('---------------');
            // console.log(getSelectedPartyData);
            // console.log('---------------');
            console.log(selectedPartyData.players);
            console.log('+------- FIN DES DATAS ---------+');
            body = {
                'message': `User ${selectedPartyData.name} updated and status unmodered `,
            }
        }
        catch (error) {
            status = 404;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }


    /**
     * dev test function, will be removed
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async start(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let party = await Party.findById(id);
            body = {
                'message': `Detail from ${party.name}`,
                party
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }
}

export default PartyController;
