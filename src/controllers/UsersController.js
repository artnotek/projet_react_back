import User from "../models/User";
import Party from "../models/Party";
import Jwt from "jsonwebtoken"


class UsersController {
    /**
     * function to register a new user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async create(req,res){
        let status = 200;
        let body = {};

        try {
            console.log('bonjour back');
            // checker si l'utilisateur a le bon mot de passe et email
            console.log(req.body);
            let user = await User.create({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                surname: req.body.surname,
                password: req.body.password
            });
            body = {
                'message': `User ${user.firstname} created`,
                user
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);

    }


    /**
     * function to find all user created from the DB
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async findAll(req, res){
        let status = 200;
        let body = {};

        try {
            let users = await User.find();
            body = {
                users,
                'message': 'User list'
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    /**
     * function to display all user current information
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let user = await User.findById(id);
            body = {
                'message': `Detail from ${user.firstname}`,
                user
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    /**
     * function to update current user informations
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try {
            let user = await User.findById(req.params.id);
            console.log(req.params.id);
            await user.update({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                surname: req.body.surname,
                password: req.body.password,
                isModerated: false
            });

            body = {
                'message': `profile updated`,
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * function to delete current user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let user = await User.findByIdAndRemove(id);
            //await User.remove({_id: req.params.id});
            body = {
                'message': `user deleted`
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    /**
     * function to allow an user access to current app
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async auth(req,res){
        let status = 200;
        let body = {};
        try {
            console.log('bonjour back');
            // checker si l'utilisateur a le bon mot de passe et email
            console.log(req.body);
            let user = await User.findOne({surname: req.body.surname});
            console.log(req.body.surname);
            console.log('------');

            console.log(user);
            console.log('------');
            console.log(user.password === req.body.password);
            if(user && user.password === req.body.password){
                let token = Jwt.sign({sub: user._id}, "monsecret");
                body = {
                    user,
                    token
                }
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * dev test function, will be removed
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try {
            // récupération de tout le contenu simplement
            //let users = await User.find();

            // exemple de limitation de résultat, ici on affiche uniquement les prénom
            //let users = await User.find().select('firstname');

            // exemple de limitation en RETIRANT un champs du résultat
            let users = await User.findAll();
            // let users = await User.find().select('firstname');

            // find()
            // findById()
            // findOne({email: 'content'})


            body = {
                users,
                'message': 'User list'
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }
}

export default UsersController;
