import mongoose from 'mongoose';

const commentSchema = new mongoose.Schema({
    comment: {
        type: String,
        required: true
    },
    rawDrawingInfo: String
});

const Comment = mongoose.model('Comment', commentSchema);
export default Comment;