import mongoose from 'mongoose';

const partySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    theme: {
        type: String,
        required: true
    },
    creatorId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
        // type: String,
        // required: true,
        // unique: true
    },
    players: [
        {
            userId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            isModerated: {
                type: Boolean,
                default: false
            }
        }
        ],
    drawings: [
        {
            drawingLocationId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Drawing"
            },
            userId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "User"
            },
            isModerated: {
                type: Boolean,
                default: false
            },
            comments: [
                {
                    commentLocationId: {
                        type:mongoose.Schema.Types.ObjectID,
                        ref: "Comment"
                    },
                    userId: {
                        type: mongoose.Schema.Types.ObjectId,
                        ref: "User"
                    },
                    isModerated: {
                        type: Boolean,
                        default: false
                    }
                }
            ]
        }
    ],
    isActive: {
        type: Boolean,
        default: true
    }
});

const Party = mongoose.model('Party', partySchema);
export default Party;