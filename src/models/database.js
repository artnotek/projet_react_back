import mongoose from 'mongoose';

const connectDb = () =>{
    let connection = null;

    connection = mongoose.connect('mongodb://localhost:27017/Sketch', {useNewUrlParser: true});
    return connection;
}

export default {connectDb};