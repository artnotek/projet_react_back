import {Router} from 'express';
import UsersController from "../controllers/UsersController";
import PartyController from "../controllers/PartyController";
import DrawingController from "../controllers/DrawingController";

const router = Router();

router.get('/', function (req, res) {
    res.send("<h1>hello world</h1>");
});


//User routes
// router.get('/users/authentificate', UsersController.auth);
router.post('/users/authentificate', UsersController.auth);
router.get('/users', UsersController.findAll);
router.post('/users', UsersController.create);
router.get('/users/:id', UsersController.details);
router.put('/users/:id', UsersController.update);
router.delete('/users/:id', UsersController.delete);

//Party routes
// router.get('/')
router.post('/newgame', PartyController.createNewSession);
router.post('/joingame', PartyController.getIntoGame);
router.get('/joingame', PartyController.findAll);
router.get('/joingame/:id', PartyController.details);
router.put('/hideselecteduser', PartyController.moderateSelectedUser);
router.put('/unhideselecteduser', PartyController.unModerateSelectedUser);
router.put('/hideselecteddrawing', PartyController.moderateSelectedDrawing);
router.put('/unhideselecteddrawing', PartyController.unModerateSelectedDrawing);
router.put('/endparty', PartyController.endSession);
router.put('/unmoderatename', PartyController.unmoderateUserName);

//Drawing routes
router.post('/drawing', DrawingController.create);
router.get('/drawing/:id', DrawingController.details);
export default router;